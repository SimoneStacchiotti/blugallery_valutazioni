'use strict';

/*
Constants used in the functions to ensure consistency
Adjust values to fit your desired naming and time frame conventions.
*/
var bearerToken = 'Bearer 6bfec2ba14979600cf39d4eb';
var firstTime = true;
var FOLDER_NAME = 'post_requests';
const VERSION = '1.0',
    CACHE_NAME = "BluGalleryFeedback_cache-v" + VERSION,
    IDB_VERSION = VERSION,
    PRECACHE_URLS = [
        "manifest.json",
        "index.html",
        "home.html",
        "thanks.html",
        "faces.html",
        "prestazioni.html",
        "404.html",
        "js/app.js",
        "img/indexBG-960x960.png",
        "img/logo-500x188.png",
        "css/main.css",
        "https://www.centromedicoblugallery.it/DesktopModules/GestionePrenotazioni/API/Centri/SaveFeedback"
];
        //"https://blugallerywebapi.e-lios.eu/api/Prestazioni";
        //"https://blugallerywebapi.e-lios.eu/api/Valutazioni",

var fixedHeader = new Headers();
fixedHeader.append('Authorization', 'Bearer 6bfec2ba14979600cf39d4eb');
fixedHeader.append('Accept','application/json');
fixedHeader.append('Content-Type','application/json');
fixedHeader.append('Access-Control-Allow-Origin', '*');

var seconds = 84600; // 24h
var our_db;
openDatabase();

self.addEventListener('install', e => {
    fetchGetMethodWithToken();

    e.waitUntil(
        caches.open(CACHE_NAME)
        .then(cache => {
          console.log('SW caching files');
          cache.addAll(PRECACHE_URLS);
        })
        .then(() => self.skipWaiting())
    );
});

self.addEventListener('activate', function(e) {
    e.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(cache => {
        
                    if(cache !== CACHE_NAME) {
                        console.log('SW clearing old cache');
                        return caches.delete(cache);
                    }
                })
            );
        })
    );
});

self.addEventListener('fetch', function(event) {
    // every request from our site, passes through the fetch handler
    // I have proof
    if (event.request.clone().method === 'GET') {
        event.respondWith(
            caches.match(event.request).then(
                function(response) {
                    return response || fetch(event.request).then(
                        function(response) {
                            let cacheResp = response.clone();
    
                            if ([0, 200].includes(response.status) &&
                                event.request.url.indexOf("chrome-extension")) {
                                
                                caches.open(CACHE_NAME)
                                .then(function(cache) {
                                    cache.put(event.request, cacheResp);
                                });
                            }
                            return response;
                        }
                    )
                    .catch(error => {
                        console.error(error, event.request);
                    })
                }
            )
        );
    } 
    else if (event.request.clone().method === 'POST') {
        // get payload data of request
        event.request.clone().text().then(function(body) {
            if(our_db !== undefined && !navigator.onLine) {
                // attempt to send request normally
                savePostRequests(event.request.clone().url, body);
            }
        });
        
        /* if(!navigator.onLine) {
            event.respondWith(fetch(event.request.clone()).catch(function(error) 
            {
                console.log('respondWith');
            }));
        } */
    }
});

self.addEventListener('sync', function (event) {
    var d = Date(Date.now()); 
    console.log('now online ' + d.toString());

    if (event.tag === 'sendFormData' && our_db !== undefined) { 
        event.waitUntil(
            // Send our POST request to the server, now that the user is
            // online
            sendPostToServer()
        );
    }
})

function sendPostToServer () {
    var savedRequests = [];
    var req = getObjectStore(FOLDER_NAME).openCursor();

    req.onsuccess = async function (event) {
        var cursor = event.target.result

        if (cursor) {
        // Keep moving the cursor forward and collecting saved
        // requests.
        savedRequests.push(cursor.value)
            cursor.continue()
        } 
        else {
            // At this point, we have collected all the post requests in
            // indexedb.
            for (let savedRequest of savedRequests) {
                
                var request = new Request(savedRequest.url, {
                    method: savedRequest.method,
                    mode: savedRequest.mode,
                    headers: fixedHeader,
                    body: savedRequest.body
                });

                // send them to the server one after the other
                fetch(request)
                .then(function (response) {
                    console.log('server response', response)
                    if (response.status < 400) {
                    // If sending the POST request was successful, then
                    // remove it from the IndexedDB.
                    getObjectStore(FOLDER_NAME,
                        'readwrite').delete(savedRequest.id)
                    } 
                }).catch(function (error) {
                    // This will be triggered if the network is still down. 
                // The request will be replayed again
                // the next time the service worker starts up.
                console.error('Send to Server failed:', error)
                // since we are in a catch, it is important an error is
                //thrown,so the background sync knows to keep retrying 
                // the send to server
                throw error
                })
            }
        }
    }
}

function fetchGetMethodWithToken () {
    var uri = "https://www.centromedicoblugallery.it/DesktopModules/GestionePrenotazioni/API/Centri/GetCentri";

    var requestWithToken = new Request(uri, {
        method: 'get',
        mode: 'cors',
        headers: fixedHeader
    });

    fetch(requestWithToken).then(function(response){
        caches.open(CACHE_NAME)
        .then(function(cache) {
            cache.put(requestWithToken, response.clone());
        });
    })
}

function getObjectStore (storeName, mode) {
    // retrieve our object store
    return our_db.transaction(storeName,mode).objectStore(storeName);
}

function savePostRequests (url, body) {
    // get object_store and save our payload inside it
    var request = getObjectStore(FOLDER_NAME, 'readwrite').add({
        url: url,
        body: body,
        method: 'POST'
    });

    request.onsuccess = function (event) {
        console.log('new POST request saved in indexedb -> ' + request);
    }

    request.onerror = function (error) {
        console.error(error);
    }
}

self.addEventListener('message', function (event) {
    console.log(event.data);
    if (event.data.hasOwnProperty('form_data')) {
        // receives form data from script.js upon submission
        var form_data = event.data.form_data;
    }
})

function openDatabase () {
    // if `flask-form` does not already exist in our browser (under our site), it is created
    var indexedDBOpenRequest = indexedDB.open('flask-form',
    IDB_VERSION)

    indexedDBOpenRequest.onerror = function (error) {
        // error creating db
        console.error('IndexedDB error:', error)
    }

    indexedDBOpenRequest.onupgradeneeded = function () {
        // This should only executes if there's a need to 
        // create/update db.
        this.result.createObjectStore('post_requests', {
        autoIncrement:  true, keyPath: 'id' })
    }

    // This will execute each time the database is opened.
    indexedDBOpenRequest.onsuccess = function () {
        our_db = this.result
    }
}

self.addEventListener("push", e => {
    console.log('[SW] Push Received.');
    console.log(`[SW] Push had this data: "${e.data.text()}"`);
    try {
        const title = "AGGIORNAMENTO CACHE";
        const options = {
            body: `La cache interna dell'app è stata ripulita. Assicurarsi di essere connessi alla rete 
            prima di proseguire nell'utilizzo dell'applicazione`,
            icon: 'img/feed-rss-56x56.png',
            badge: 'img/feed-rss-56x56.png',
            image: 'img/feed-rss-56x56.png',
            vibrate: [200, 100, 200, 100, 200, 100, 200]
        };
        e.waitUntil(self.registration.showNotification(title, options));
    } catch (err) {
        console('invalid json - notification supressed');
    }
});

async function sendAsyncPostToServer() {

}

refreshCacheAsync();

async function refreshCacheAsync() {
    console.log('set time refresh cache: ' + seconds + ' seconds');

    // first, send data to DB
    if(our_db !== undefined) {
        sendPostToServer();
    }

    return new Promise((event) => {
        setTimeout(() => {
            caches.keys().then(cacheNames => {
                return Promise.all(
                    cacheNames.map(function(CACHE_NAME) {
                        console.log(CACHE_NAME + ' deleted');
                        return caches.delete(CACHE_NAME);
                    })
                );
            });

            const title = 'Aggiornamento App Cache';
            const options = {
            body: `La cache interna dell'app è stata ripulita. 
            Assicurarsi di essere connessi alla rete prima di proseguire nell'utilizzo dell'applicazione.
            La cache viene ripulita ogni minuto`
            };

            //self.registration.showNotification(title, options);

            /* CICLO REFRESH */
            refreshCacheAsync();

        }, 1000 * seconds);
    });
}
