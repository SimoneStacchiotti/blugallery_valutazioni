if(navigator.serviceWorker) {
    navigator.serviceWorker.register('./sw.js')
    .then(function() {
        return navigator.serviceWorker.ready
    })
    .then(function(registration) {
            registration.sync.register('sendFormData')
            .catch(function(err) {
                console.error('registration.sync.register -> ' + err);
            })
    })
    .catch(function(err) {
        console.error('sw registration CATCH -> ' + err);
    })
}

var deferredPrompt;

var fixedHeader = new Headers();
fixedHeader.append('Authorization', 'Bearer 6bfec2ba14979600cf39d4eb');
fixedHeader.append('Accept','application/json');
fixedHeader.append('Content-Type','application/json');
fixedHeader.append('Access-Control-Allow-Origin', '*');

window.addEventListener("beforeinstallprompt", function (e) {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();
    // Stash the event so it can be triggered later.
    e.userChoice.then(function(outcome) 
    { 
        console.log(outcome); // either "accepted" or "dismissed"
    });
    deferredPrompt = e;
    //showAddToHomeScreen();
    document.getElementById('btnAd2hs').addEventListener('click', addToHomeScreen);
    
    $('#btnAd2hs').show();
});

function addToHomeScreen() {
    if (deferredPrompt) 
    {
        // Show the prompt
        deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        deferredPrompt.userChoice
        .then(function (choiceResult) 
        {
            if (choiceResult.outcome === 'accepted') {
                // hide our user interface that shows our A2HS button
                console.log('User accepted the A2HS prompt');
            } else {
                console.log('User dismissed the A2HS prompt');
            }
            deferredPrompt = null;
        });
    }
}

function getCentri() {

    //http://localhost:64304
    //https://blugallerywebapi.e-lios.eu/api/Prestazioni
    //https://www.centromedicoblugallery.it/DesktopModules/GestionePrenotazioni/API/Centri/GetCentri
    const uri = 'https://www.centromedicoblugallery.it/DesktopModules/GestionePrenotazioni/API/Centri/GetCentri';

    var request = new Request(uri, {
        method: 'get',
        mode: 'cors',
        headers: fixedHeader
    });

    let output = '';
    let elementPerPage = 12;
    let elementsPerRow = 4;
    let everyPerPage = 0;
    let items = [];

    fetch(request)
    .then((response) => {
        if(response.ok) {
            return response.json();
        } else {
            getError(response);
            return;
        }
    })
    .then((data) => {
        getShortNames(data);
        data.forEach(function(prestazione) {
            everyPerPage++;
            output = `
            <div class="btn btn-light prestazione" data-toggle="buttons">
                <input id="${prestazione.idCentro}" type="hidden">
                <div class="center font-weight-bold">${prestazione.nomeCentro}</div>
            </div>
            `;

            items.push(output);
            
            /* PAGINAZIONE */
            if(everyPerPage <= elementPerPage) {
                $('.owl-item #first-container').append(output);

                if(everyPerPage == elementsPerRow) {
                    $('.owl-item #first-container').append(`<br>`);
                }
                
            }

            if(everyPerPage > elementPerPage && everyPerPage <= elementPerPage * 2) {
                $('.owl-item #second-container').append(output);

                if(everyPerPage == elementsPerRow * 2) {
                    $('.owl-item #second-container').append(`<br>`);
                }
            }

            if(everyPerPage > elementPerPage * 2 && everyPerPage <= elementPerPage * 3) {
                $('.owl-item #third-container').append(output);

                if(everyPerPage == elementsPerRow * 3) {
                    $('.owl-item #third-container').append(`<br>`);
                }
            }
        });
    })
    .catch(function(error) 
    {
        console.error('CATCH getCentri -> ' + error);
    })
}

function addValutazione(feedback) {

    //http://localhost:64304
    //https://blugallerywebapi.e-lios.eu/api/Valutazioni
    //https://www.centromedicoblugallery.it/DesktopModules/GestionePrenotazioni/API/Centri/SaveFeedback  

    var uri = 'https://www.centromedicoblugallery.it/DesktopModules/GestionePrenotazioni/API/Centri/SaveFeedback';

    var jsonData = 
    {
        "id": feedback.id,
        "idCentro": feedback.idCentro,
        "voto": feedback.voto,
        "data": feedback.data
    }

    var request = new Request(uri, {
        method: 'post',
        mode: 'cors',
        headers: fixedHeader,
        body: JSON.stringify(jsonData)
    });

    fetch(request)
    .then(function(response) {
        console.log(response);
        if(checkStatus(response)) {
            return response;
        } else {
            getError(response);
            return;
        }
    })
    .catch(function(error) {
        console.error('CATCH addValutazione -> ' + error);
    })
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response
    } 
    else {
        getError(response);
        throw error
    }
}

function AddZero(num) {
    return (num >= 0 && num < 10) ? "0" + num : num + "";
}

function getError(response) {
    let error = '';

    $('#home').remove();

    // UNAUTHORIZED
    if(response.status == 401)
    {
        error += `
        <div class="btn btn-light error animated fadeIn">
            <i class="far fa-times-circle fa-10x" style="font-weight: 800"></i>
            <h3 style="font-size: 60px !important;"> Errore </h3>
            <p style="font-size: 30px !important;">
            Salvataggio non autorizzato
            </p>
        </div>
        `;
    }
    // internal server error
    if(response.status == 500)
    {
        error += `
        <div class="btn btn-light error animated fadeIn">
            <i class="far fa-times-circle fa-10x" style="font-weight: 800"></i>
            <h3 style="font-size: 60px !important;"> Errore interno del server </h3>
        </div>
        `;
    }
    // method not allowed
    if(response.status == 405)
    {
        error += `
        <div class="btn btn-light error animated fadeIn">
            <i class="far fa-times-circle fa-10x" style="font-weight: 800"></i>
            <h3 style="font-size: 60px !important;"> Errore </h3>
            <p style="font-size: 45px !important;">
            Funzione non autorizzata
            </p>
        </div>
        `;
    }
    /*
    else
    {
        error += `
        <div class="btn btn-light error animated fadeInUp">
            <i class="far fa-times-circle fa-10x" style="font-weight: 800"></i>
            <h3 style="font-size: 40px !important;"> Nessuna connessione a Internet </h3>
            <br>
            <p style="font-size: 28px !important;">
            Controllare che la connessione sia presente e ricaricare l'applicazione
            </p>
        </div>
        `;
    }
    */
    
    /* $.notify({
        icon: 'glyphicon glyphicon-warning-sign',
        message: 'ERRORE',
        target: '_blank'
    },{
        element: 'body',
        position: null,
        type: "danger",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "bottom",
            align: "center"
        },
        offset: 20,
        spacing: 40,
        z_index: 1031,
        delay: 3000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
    }); */

    document.getElementById('output-container').innerHTML = error;
}

function getShortNames(data) {
    data.forEach(function(centro) {
        switch(centro.idCentro) {
            case 1:
                centro.nomeCentro = 'Radiologia';
                break;
            case 2:
                centro.nomeCentro = 'Cardiologia - Medicina dello sport';
                break;
            case 3:
                centro.nomeCentro = 'Ortopedia';
                break;
            case 4:
                centro.nomeCentro = 'Centro di Osteoporosi';
                break;
            case 5:
                centro.nomeCentro = 'Gastroenterologia ed Endoscopia Digestiva';
                break;
            case 6:
                centro.nomeCentro = 'Otorinolaringoiatria';
                break;
            case 7:
                centro.nomeCentro = 'Oculista';
                break;
            case 8:
                centro.nomeCentro = 'Dermatologia e Medicina Estetica';
                break;
            case 9:
                centro.nomeCentro = 'Allergologia';
                break;
            case 10:
                centro.nomeCentro = 'Psichiatria';
                break;
            case 11:
                centro.nomeCentro = 'Urologia';
                break;
            case 12:
                centro.nomeCentro = 'Biologia della Nutrizione';
                break;
            case 13:
                centro.nomeCentro = 'Ecografia Internistica';
                break;
            case 14:
                centro.nomeCentro = 'Geriatria e Medicina Interna';
                break;
            case 15:
                centro.nomeCentro = 'Chirurgia Vascolare e Flebologia';
                break;
            case 16:
                centro.nomeCentro = 'Ginecologia ed Ostetricia';
                break;
            case 17:
                centro.nomeCentro = 'Endocrinologia';
                break;
            case 18:
                centro.nomeCentro = 'Diabetologia';
                break;
            case 20:
                centro.nomeCentro = 'Psicologia';
                break;
            case 22:
                centro.nomeCentro = 'Riabilitazione Morfofunzionale';
                break;
            case 23:
                centro.nomeCentro = 'Reumatologia';
                break;
            case 24:
                centro.nomeCentro = 'Neurologia';
                break;
            case 25:
                centro.nomeCentro = 'Medicina Legale';
                break;
            case 1002:
                centro.nomeCentro = 'Spazio Nascita';
                break;
            case 1003:
                centro.nomeCentro = 'Pediatria';
                break;
            case 1004:
                centro.nomeCentro = 'Agopuntura e Mesoterapia';
                break;
            case 1005:
                centro.nomeCentro = 'Consulenze ortopediche ambulatoriali';
                break;
            case 1006:
                centro.nomeCentro = 'Pneumologia';
                break;
            case 1010:
                centro.nomeCentro = 'Idrocolonterapia';
                break;
            case 1011:
                centro.nomeCentro = 'Centro Studio e Cura della tiroide';
                break;
            case 1013:
                centro.nomeCentro = 'Diagnosi prenatale';
                break;
        }
    })

    return data;
}